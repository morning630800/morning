<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-17 07:25:45
 * @Organization: Knockout System Pvt. Ltd.
 */

ob_start();
session_start();
include 'admin/include/config.php';
include 'admin/include/dbconnect.php';

include 'admin/include/function.php';


if(isset($_POST['submit']) && $_POST['submit']!=""){

	/*Login validation codes*/
	//$conn->real_escape_string();

	
	$post_username = sanitizeString($_POST['username']);
	/*debugger($post_username, true);*/
	
	$post_password = md5($_POST['password']);

	$user = getUserByUsername($post_username);
	if($user){
		
		if($user['password'] == $post_password){
			$_SESSION['user_id'] = $user['id'];
			$_SESSION['name'] = $user['full_name'];
			$_SESSION['role_id'] = $user['role_id'];
			$_SESSION['image'] = "http://faker.com/200/200";
			
			$_SESSION['success'] = "Welcome! ".$_SESSION['name']." to System";

			@userLoginUpdateById($user['id']);


			if(isset($_POST['remember_me']) && $_POST['remember_me'] == 1){
				setcookie('username',$_SESSION['name'],time()+86400);
				setcookie('role_id',$_SESSION['role_id'],time()+86400);
			}


			@header('location: admin/dashboard.php');
			exit;
		} else {
			$_SESSION['error'] = "Your password does not match";

			@header('location: ../login.php');
			exit;
		}
	} else {
		$_SESSION['error'] = "Your username does not exists";

		@header('location: ../login.php');
		exit;
	}
} else {
	$_SESSION['error'] = "Illegal Entry";
	header('Location: ../login.php');
	exit;
}
ob_flush();
?>
