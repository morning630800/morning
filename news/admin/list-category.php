<?php 
session_start(); 
$pagename = "Dashboard";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}
?>
<?php include 'include/header.php'; ?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            List Category
                        </h1>
                    </div>

                    <div class="col-md-12 col-lg-12">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <th>S.N</th>
                                <th>Title</th>
                                <th>Status</th>
                                <th>Summary (150 chars..)</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <?php 
                                    $allCategory = getAllCategory();
                                    if($allCategory){
                                        $i = 1;
                                        foreach($allCategory as $categories){
                                    ?>
                                        <tr>
                                            <td><?php echo $i;?></td>
                                            <td><?php echo $categories['category_title'];?></td>
                                            <td><?php echo getStatus($categories['status']);?></td>
                                            <td>
                                                <?php 
                                                echo substr($categories['summary'],0,150)."...";
                                                ?>
                                            </td>
                                            <td>
                                                <a href="add-category.php?id=<?php echo $categories['id'];?>&amp;act=<?php echo substr(md5('edit-'.$categories['id']),0,10);?>">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <a onclick="return confirm('Are you sure you want to delete this category?')" href="category-add-process.php?id=<?php echo $categories['id'];?>&amp;act=<?php echo substr(md5('del-'.$categories['id']),0,10);?>">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php 
                                        $i++;
                                        }
                                    } else {
                                    ?>
                                    <tr>
                                        <td colspan="5">Sorry! There are no any categories in the table.</td>
                                    </tr>
                                    <?php
                                    } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>