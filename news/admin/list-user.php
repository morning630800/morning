<?php 
session_start(); 
$pagename = "Dashboard";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}
?>
<?php include 'include/header.php'; ?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            List User
                        </h1>
                    </div>

                    <div class="col-md-12 col-sm-12 col-lg-12">
                    	<table class="table table-bordered table-stripped">
                    		<thead>
                    			<th>S.N</th>
                    			<th>Full Name</th>
                    			<th>Username</th>
                    			<th>Email Address</th>
                    			<th>User Type</th>
                    			<th>Status</th>
                    			<th>Action</th>
                    		</thead>
                    		<tbody>
                    			<?php
                    				$users = getAllUser();
                    				if($users){
                    					$i=1;
                    					foreach($users as $user){
                                            /*debugger($user,true);*/
                						?>
                						<tr>
                							<td><?php echo $i;?></td>
                							<td><?php echo $user['full_name'];?></td>
                							<td><?php echo $user['username'];?></td>
                							<td><?php echo $user['email_address'];?></td>
                							<td>
                							<?php 
                								echo getUserType($user['role_id']);
                							?>
                							</td>
                							<td>
                							<?php 
                								echo getStatus($user['status']);
                							?>
                							</td>
                							<td>
                								<a href="#">Edit </a>
                								<a href="#">Delete</a>
                							</td>
                						</tr>
                						<?php
                						$i++;
                    					}

                    				} else {
                    					echo '<tr>';
                    					echo '<td colspan="7">Sorry! There are no any users in the database.</td>';
                    					echo '</tr>';
                    				}
                    			?>
                    		</tbody>
                    	</table>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>