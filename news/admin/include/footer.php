<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-22 07:32:22
 * @Organization: Knockout System Pvt. Ltd.
 */
?>
    <!-- jQuery -->
    <script src="<?php echo JS_URL;?>jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo JS_URL;?>bootstrap.min.js"></script>

</body>

</html>
