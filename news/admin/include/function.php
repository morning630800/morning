<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-21 07:04:21
 * @Organization: Knockout System Pvt. Ltd.
 */
function debugger($array, $isDie = false){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

function argument_by_value($value){
	$value = $value+1;
	return $value;
}


function volume_calculate($length, $width=0, $height=0){

	if($width > 0 && $height > 0){
		$volume = $length*$width*$height;
	} else {
		$volume = $length*$length*$length;
	}

	return $volume;
}


function current_time(){
	return date('Y-M-d H:i:s A');
}

function previous_date($days){
	$xyz = strtotime(date('Y-m-d H:i:s A')." -".$days." days");

	$date = date('Y-m-dS z H:i:s A',$xyz);
	return $date;
}

function sanitizeString($string){
	global $conn;
	return mysqli_real_escape_string($conn,$string);
}

function getUserByUsername($username){
	global $conn;
	$sql = "SELECT * FROM users WHERE username = '".$username."'";

	$query = mysqli_query($conn,$sql);

	if(mysqli_num_rows($query) > 0){
		/*echo "here";
		exit;*/

		$data = mysqli_fetch_assoc($query);
		/*debugger($data, true);*/
		
		return $data;
	} else {
		return false;
	}
}

function userLoginUpdateById($userId){
	global $conn;
	$sql = "UPDATE users SET last_login = now(), login_ip = '".$_SERVER['REMOTE_ADDR']."'";
	$query = mysqli_query($conn,$sql);
	if($query){
		return true;
	} else {
		return false;
	}
}

function addUser($data, $isDie=false){
	global $conn;
	$sql = "INSERT INTO users SET full_name = '".$data['full_name']."', username = '".$data['username']."', password = '".$data['password']."', status = ".$data['status'].", role_id = ".$data['role_id'].", added_date = now() ";
	if($isDie){
		echo $sql;
		exit;
	}
	debugger($conn,true);

	$query = mysqli_query($conn,$sql);
	/*debugger($query,true);*/

	if($query){

		return mysqli_insert_id($conn);
	} else {
		return false;
	}
}

function getAllUser($isDie=false){
	global $conn;
	$sql = "SELECT * FROM users ORDER BY id DESC";
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn,$sql);
	if(mysqli_num_rows($query) <= 0){
		return false;
	} else {
		$data = array();
		while($row = mysqli_fetch_assoc($query)){
			$data[] = $row;
		}
		return $data;
	}

}

function getUserType($role_id){
	if($role_id == 1){
		return "Admin";
	} else if($role_id == 2){
		return "User";
	} else {
		return "User type not defined";
	}
}

function getStatus($status){
	if($status == 1){
		return "Active";
	} else if($status == 0){
		return "Inactive";
	} else {
		return "Suspended";
	}
}

function addCategory($data, $isDie=false){
	global $conn;
	
/*	debugger($data,true);*/	
	$sql = "INSERT INTO categories SET 
						category_title = '".$data['category_title']."',
						summary = '".$data['summary']."',
						description = '".$data['description']."',
						status = ".$data['status'].",
						added_date = now(),
						added_by = ".$data['added_by'];
	/*echo $sql;
	exit;
	*/
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn, $sql);
	/*debugger($conn,true);*/

	if($query){
		return mysqli_insert_id($conn);
	} else {
		return false;
	}
}

function getAllCategory($status = 0){
	global $conn;
	if($status == 0){
		$sql = "SELECT * FROM categories ORDER BY id DESC";
	} else {
		$sql = "SELECT * FROM categories WHERE status = ". $status." ORDER BY category_title ASC";
	}

	$query = mysqli_query($conn,$sql);
	if(mysqli_num_rows($query) <=0){
		return false;
	} else {
		$data = array();
		while($row = mysqli_fetch_assoc($query)){
			$data[] =$row;
		}
		return $data;
	}
}

function addNews($data, $isDie=false){
	global $conn;

	$sql = "INSERT INTO news SET 
				category_id = ".$data['category_id'].",
				title = '".$data['title']."',
				summary = '".$data['summary']."',
				description = '".$data['description']."',
				reporter = '".$data['reporter']."',
				location ='".$data['location']."',
				status = ".$data['status'].",
				image = '".$data['image']."',
				news_date = '".$data['news_date']."',
				added_date = now(),
				added_by = ".$_SESSION['user_id'];
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn,$sql);
	if($query){
		return mysqli_insert_id($conn);
	} else {
		return false;
	}
}


function getAllNews($status =0, $isDie = false){
	global $conn;
	if($status){
		$sql = "SELECT news.*, categories.category_title FROM news LEFT JOIN categories ON news.category_id = categories.id WHERE news.status = ".$status." ORDER BY news.id DESC";
	} else {
		$sql = "SELECT news.*, categories.category_title FROM news LEFT JOIN categories ON news.category_id = categories.id ORDER BY news.id DESC";
	}
	$query = mysqli_query($conn,$sql);
	if(mysqli_num_rows($query) <=0){
		return false;
	} else {
		$data = array();
		while($row = mysqli_fetch_assoc($query)){
			$data[] = $row;
		}
		return $data;
	}
}


function getCategoryById($cat_id, $isDie=false){
	global $conn;
	$sql = "SELECT * FROM categories WHERE id = ".$cat_id;
	if($isDie){
		echo $sql;
		exit;
	} else {
		$query = mysqli_query($conn,$sql);
		if(mysqli_num_rows($query) <= 0){
			return false;
		} else {
			$data = mysqli_fetch_assoc($query);
			return $data;
		}
	}
}

function deleteData($table, $field, $value){
	global $conn;
	$sql = "DELETE FROM ".$table." WHERE ".$field." = '".$value."'";
	$query = mysqli_query($conn,$sql);
	if($query){
		return true;
	} else {
		return false;
	}
}

function updateCategory($data, $isDie = false){
	global $conn;
	$sql = "UPDATE categories SET 
						category_title = '".$data['category_title']."',
						summary = '".$data['summary']."',
						description = '".$data['description']."',
						status = ".$data['status'].",
						updated_date = now(),
						added_by = ".$data['added_by']."
						 WHERE id = ".$data['category_id'];
	/*echo $sql;
	exit;
	*/
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn, $sql);
	/*debugger($conn,true);*/

	if($query){
		return true;
	} else {
		return false;
	}
}


function changeDateFormat($date, $format){
	//$formt = "Y-m-d h:i:s A";
	$converted_date = date($format, strtotime($date));
	return $converted_date;
}
?>
