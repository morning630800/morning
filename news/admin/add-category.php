<?php 
session_start(); 
$pagename = "Category Add";
include 'include/header.php';
if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: ../login.php');
    exit;
}

$act = 'add';

if(isset($_GET['id']) && isset($_GET['act']) && $_GET['act'] == substr(md5('edit-'.$_GET['id']),0,10)){
    $act = 'update';

    $id = sanitizeString($_GET['id']);
    $category_info = getCategoryById($id);
    if(!$category_info){
        $_SESSION['warning'] = "Data does not exists";
        @header('location: list-category.php');
        exit;
    }
} else if(isset($_GET['id']) && isset($_GET['act']) && $_GET['act'] != substr(md5('edit-'.$_GET['id']),0,10)){
    $_SESSION['error'] = "Sorry! The action you specified does not exist.";
    @header('location: list-category.php');
    exit;
}


?>    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo ucfirst($act);?> Category
                        </h1>
                    </div>
                    <div class="col-md-12 col-lg-12">
                    	<form method="post" action="category-add-process.php">
                    		<div class="form-group">
                    			<label>Cateogry Title</label>
                    			<input type="text" name="category_title" class="form-control" required id="category_title" placeholder ='Enter title' value="<?php echo (isset($category_info['category_title'])) ? $category_info['category_title'] : '';?>" />
                    		</div>
                    		<div class="form-group">
                    			<label>Cateogry Summary</label>
                    			<textarea name="category_summary" class="form-control" id="category_summary" rows="5" style="resize:  none;"><?php echo (isset($category_info['summary'])) ? $category_info['summary'] : '';?></textarea>
                    		</div>
                    		<div class="form-group">
                    			<label>Cateogry Description</label>
                    			<textarea name="category_description" class="form-control" id="category_description" rows="5" style="resize:  vertical ;"><?php echo (isset($category_info['description'])) ? $category_info['description'] : '';?></textarea>
                    		</div>
                    		<div class="form-group">
                    			<label>Status</label>
                    			<select name="status" required class="form-control" id="status">
                    				<option value="1" <?php echo (isset($category_info['status']) && $category_info['status'] == 1) ?'selected' : '';?>>Active</option>
                    				<option value="0" <?php echo (isset($category_info['status']) && $category_info['status'] == 0) ?'selected' : '';?>>Inactive</option>
                    			</select>
                    		</div>
                    		<div class="from-group">
                                <input type="hidden" name="category_id" value="<?php echo (isset($category_info) && $category_info['id']!='') ? $category_info['id'] : '';?>" />
                    			<input type="submit" name="submit" class="btn btn-success" value="<?php echo ucfirst($act);?> Category" />
                    		</div>
                    	</form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>