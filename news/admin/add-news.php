<?php 
session_start(); 
$pagename = "Dashboard";
$act = "add";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}

/*Get method*/
?>
<?php include 'include/header.php'; ?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add News 
                        </h1>
                    </div>
                    <div class="col-md-12 col-lg-12">
                    	<form method="post" action="add-news-process.php" enctype="multipart/form-data">
                    		<div class="form-group">
                    			<label>News Title</label>
                    			<input type="text" name="title" class="form-control" required id="title" />
                    		</div>                    		
                    		<div class="form-group">
                    			<label>Category</label>
                    			<?php 
                    				$all_category = getAllCategory(1);
                    				/*debugger($all_category,true);*/
                    			?>
                    			<select class="form-control" name="category_id" required id="category_id">
                    				<?php 
                    				if(isset($all_category) && $all_category!=""){
                    					foreach($all_category as $category){
               							?>
               								<option value="<?php echo $category['id'];?>">
               									<?php echo $category['category_title'];?></option>
                						<?php
                    					}
                    				}
                    				?>
                    			</select>
                    		</div>
                    		<div class="form-group">
                    			<label>Summary</label>
                    			<textarea name="summary" id="sumamry" required class="form-control" rows="5" style="resize: none;"></textarea>
                    		</div>
                    		<div class="form-group">
                    			<label>Description</label>
                    			<textarea name="description" id="description" required class="form-control" rows="5" style="resize: vertical;"></textarea>
                    		</div>
                    		<div class="form-group">
                    			<label>Reporter</label>
                    			<input type="text" name="reporter" class="form-control"  id="reporter" required />
                    		</div>
                    		<div class="form-group">
                    			<label>Location</label>
                    			<input type="text" name="location" class="form-control"  id="location" required />
                    		</div>
							<div class="form-group">
                    			<label>News Date:</label>
                    			<input type="date" name="news_date" class="form-control"  id="news_date" required value="<?php echo changeDateFormat(date('Y-m-d h:i:s'),"Y-m-d");?>"/>
                    		</div>
							<div class="form-group">
                    			<label>Status</label>
                    			<select name="status" class="form-control" id="status" required>
                    				<option value="1">Active</option>
                    				<option value="0">Inactive</option>
                    			</select>
                    		</div>
                    		<div class="form-group">
                    			<label>Image</label>
                    			<input type="file" name="image" id="image" accept="*/image" />
                                <?php 
                                    if($act == "edit"){
                                        ?>
                                            <img src="<?php echo UPLOAD_URL;?>" />
                                            <input type="checkbox" name="delete_image" value="1" /> Delete this image ?
                                        <?php
                                    }
                                ?>
                    		</div>
                    		<div class="form-group">
                    			<input type="submit" name="submit" value="Add News" class="btn btn-primary" id="submit" />
                    		</div>
                    	</form>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>