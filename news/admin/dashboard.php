<?php 
session_start(); 
$pagename = "Dashboard";

if((!isset($_SESSION['user_id']) && $_SESSION['user_id'] == "") || (!isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1)){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: ../login.php');
    exit;
}
?>
<?php include 'include/header.php'; ?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dashboard
                        </h1>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>