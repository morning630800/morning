<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-31 07:09:16
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start(); 
$pagename = "Category Add";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}

include 'include/config.php';
include 'include/dbconnect.php';
include 'include/function.php';

if(isset($_POST['submit']) && $_POST['submit'] != ""){
	/*debugger($_POST,true);*/
	$data = array();
	$data['category_title'] = sanitizeString($_POST['category_title']);
	$data['summary'] = sanitizeString($_POST['category_summary']);
	$data['description'] = sanitizeString($_POST['category_description']);
	$data['status'] = sanitizeString($_POST['status']);
	$data['added_by'] = $_SESSION['user_id'];
	$data['category_id'] = sanitizeString($_POST['category_id']);

	/*debugger($data,true);*/

	if(isset($data['category_id']) && $data['category_id'] != ""){
		$act = "edit";
		$category_id = updateCategory($data);
	} else {
		$act = "add";
		$category_id = addCategory($data);
	}
	if($category_id){
		$_SESSION['success'] = "Cateogry ".$act."ed Successfully";
	} else {
		$_SESSION['error'] = "Sorry! There was problem while ".$act."ing category.";
	}
	@header('location: list-category.php');
	exit;

}

/*Delete process*/
if(isset($_GET['id']) && isset($_GET['act']) && $_GET['act']==substr(md5('del-'.$_GET['id']),0,10)){

	$id = sanitizeString($_GET['id']);
	$category = getCategoryById($id);

	if($category){
		$del = deleteData('categories','id', $id);
		if($del){
			$_SESSION['success'] = "Category Deleted Successfully.";
		} else {
			$_SESSION['error'] = "Sorry! There was problem while deleteing the category";
		}
		@header('location: list-category.php');
		exit;
	} else {
		$_SESSION['warning'] = "Data already deleted or does not exists";
		@header('location: list-category.php');
		exit;
	}

} else {
	$_SESSION['warning'] = "Invalid action specified";
	@header('location: list-category.php');
	exit;
}
?>
