<?php 
session_start(); 
$pagename = "Dashboard";

include 'include/header.php';

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}

/*$date = date('y-m-d');
$converted_date = changeDateFormat($date, "Y-m-d h:i:s");
echo $date;
echo "<br />";
echo $converted_date;
exit;*/
?>
    <div id="wrapper">

        <!-- Navigation -->
        <?php include 'include/navigation.php';?>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
<?php include 'include/notifications.php'; ?>

                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add User
                        </h1>
                        <div class="">
                        	<form method="post" name="add-user" class="form form-horizontal" action="user-add-process.php" enctype="multipart/form-data">
                        		<div class="form-group">
                        			<label>Full Name</label>
                        			<input type="text" name="full_name" class="form-control" id="full_name" required />
                        		</div>
                        		<div class="form-group">
                        			<label>User Name</label>
                        			<input type="email" name="username" class="form-control" id="username" required />
                        		</div>
                        		
                        		<div class="form-group">
                        			<label>Password</label>
                        			<input type="password" name="password" class="form-control" id="password" required />
                        		</div>
                        		<div class="form-group">
                        			<label>Status</label>
                        			<input type="radio" name="status" value="1" checked /> Active
                        			<input type="radio" name="status" value="0" /> Inactive
                        		</div>
                        		<div class="form-group">
                        			<label>User role</label>
                        			<select name="role_id" required class="form-control" id="role_id">
                        				<option value="1">Admin</option>
                        				<option value="2">User</option>
                        				<option value="3">Editor</option>
                        			</select>
                        		</div>
                        		<div class="form-group">
                        			<input type="submit" name="submit" class="btn btn-success" value="Register" id="submit" />
                        		</div>
                        	</form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php';?>