<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-01 07:05:22
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start(); 
$pagename = "News Add";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}

include 'include/config.php';
include 'include/dbconnect.php';
include 'include/function.php';

$allowed_extensions = array("jpg",'jpeg','png', 'gif');

if(isset($_POST['submit']) && $_POST['submit']!=""){
	$data = array();
	$data['title'] = sanitizeString($_POST['title']);
	$data['summary'] = sanitizeString($_POST['summary']);
	$data['category_id'] = sanitizeString($_POST['category_id']);
	$data['description'] = sanitizeString($_POST['description']);
	$data['reporter'] = sanitizeString($_POST['reporter']);
	$data['location'] = sanitizeString($_POST['location']);
	$data['news_date'] = sanitizeString($_POST['news_date']);
	$data['status'] = sanitizeString($_POST['status']);

	$image ="";
	if(isset($_FILES['image']) && $_FILES['image']['error'] == 0){
		$ext = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);
		if(in_array($ext, $allowed_extensions)){
			$image = "News-".strtotime(date('Y-m-d H:i:s'))."-".rand(0,99999).".".$ext;
			$success = move_uploaded_file($_FILES['image']['tmp_name'],'uploads/'.$image);
		} else {
			$image = "";
		}
	}
	$data['image'] = $image;


	$id = addNews($data);
	
	if($id){
		$_SESSION['success'] = "News added Successfully";
	} else {
		$_SESSION['error'] = "Sorry! There was problem while adding news";
	}
	@header("location: list-news.php");
	exit;
} else {
	$_SESSION['error'] = "Sorry! You are at wrong place";
	@header('location: list-news.php');
	exit;
}
?>
