<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-29 07:28:25
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start(); 
$pagename = "Dashboard";

if(isset($_SESSION['role_id']) && $_SESSION['role_id'] != 1){
    $_SESSION['error'] = "You are not allowed to access this page";
    @header('location: login.php');
    exit;
}

include 'include/config.php';
include 'include/dbconnect.php';
include 'include/function.php';

/*debugger($_POST,true);
*/
if(isset($_POST['submit']) && $_POST['submit'] == "Register"){
	/*debugger($_POST,true);*/
	$data = array();
	$data['full_name'] = sanitizeString($_POST['full_name']);
	$data['username'] = sanitizeString($_POST['username']);
	$data['password'] = md5($_POST['password']);
	$data['status'] = sanitizeString($_POST['status']);
	$data['role_id'] = sanitizeString($_POST['role_id']);

	/*debugger($data,true);*/

	$user = addUser($data);

	/*debugger($user,true);*/

	if($user){
		$_SESSION['success'] = "User added successfully";
		@header('location: add-user.php');
		exit;
	} else {
		$_SESSION['error'] = "There was problem while adding user";
		@header('location: add-user.php');
		exit;
	}
} else {
	$_SESSION['error'] = 'Invalide entry';
	@header('location: add-user.php');
	exit;
}
?>
