<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-22 07:36:53
 * @Organization: Knockout System Pvt. Ltd.
 */
function getAllNews($status =0, $isDie = false){
	global $conn;

	$stmt = $conn->prepare("SELECT news.*, categories.category_title FROM news LEFT JOIN categories ON news.category_id = categories.id WHERE news.status = 1 ORDER BY news.id DESC");
	$stmt->execute();
	$result = $stmt->get_result();

	if($result->num_rows <=0){
		return false;
	} else {
		$data = array();
		while($row = $result->fetch_assoc()){
			$data[] = $row;
		}
		return $data;
	}
}

function debugger($arr, $isDie=false){
	echo "<pre>";
	print_r($arr);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

function getNewsByCatId($id){
	global $conn;
	$sql = $conn->prepare("SELECT * FROM news WHERE category_id = ?");
	$sql->bind_param('i',$id);
	$sql->execute();
	$result = $sql->get_result();
	if($result->num_rows <= 0){
		return false;
	} else {
		$data = array();
		while($rows = $result->fetch_assoc()){
			$data[] = $rows;
		}
		return $data;
	}
}
?>
