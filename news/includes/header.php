<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-21 07:39:06
 * @Organization: Knockout System Pvt. Ltd.
 */
include 'includes/dbconnect.php';
include 'includes/function.php';

?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link href="assets/css/style.css" rel="stylesheet" type="text/css"  media="all" />
		<title><?php echo (isset($title) && $title != "") ? $title : 'News at 6';?></title>
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<link href='http://fonts.googleapis.com/css?family=Carrois+Gothic+SC' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="assets/js/jquery.js"></script>
	</head>
	<body>
		<!-- Start-wrap -->
		
			<!-- Start-Header-->
			
			<div class="header">
				<div class="wrap">
				<!-- Start-logo-->
				<div class="logo">
					<a href="index.html"><img src="assets/images/logo.png" title="logo" /></a>
				</div>
				
				<!-- End-Logo-->
				<!-- Start-Header-nav-->
				
				<div class="clear"> </div>
				
				<!-- End-Header-nav-->
			</div>
			</div>
			<div class="header-nav1">
				<div class="wrap">
					<ul>
					<?php
						$pages = explode("/",$_SERVER['SCRIPT_NAME']);
						/*debugger($pages,true);*/
						$current_page = array_pop($pages);
					?>
						<li class="<?php echo ($current_page == 'index.php') ? 'current' : '';?>"><a href="index.php">गृहप्रिट्स</a></li>
						<li class="<?php echo ($current_page == 'about.php') ? 'current' : '';?>"><a href="about.php">About</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Advertice</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				
				<div class="search-bar">
					<ul>
						<li><input type="text"></li>
						<li><input type="submit" value="" /></li>
					</ul>
				</div>
				</div>
				
