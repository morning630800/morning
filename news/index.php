<?php include 'includes/header.php'; ?>
<!-- End-Header-->
<div class="clear"> </div>
<!-- content-gallery-->
</div>
<div class="wrap">
    <div class="about">

        <?php include 'includes/sidebar.php'; ?>

        <div class="bloder-content">
            <?php
            $data = getAllNews();

            if ($data) {
                foreach ($data as $key => $news) {
                    ?>
                    <div class="bloger-grid">

                        <div class="blog-img">
                            <?php
                            if (isset($news['image']) && $news['image'] != "" && file_exists('uploads/' . $news['image'])) {
                                ?>
                                <img src="uploads/<?php echo $news['image']; ?>" title="img6" />
                                <?php
                            } else {
                                echo $news['title'];
                            }
                            ?>
                        </div>
                        <div class="bloger-content">
                            <h5><a href="single.php?id=<?php echo $news['id']; ?>"><?php echo $news['title']; ?></a></h5>
                            <p><?php echo $news['summary']; ?></p>
                            <ul>
                                <li><a href=""><?php echo $news['reporter']; ?></a></li>
                                <li><a href="#">: <?php echo date('Y-m-d', strtotime($news['news_date'])); ?></a></li>
                                <li><a href="single.php?id=<?php echo $news['id']; ?>"><span>Readmore</span></a></li>
                            </ul>
                        </div>
                        <div class="clear"> </div>
                    </div>

                    <?php
                    if ($key == 0) {
                        ?>
                        <div class="clear"> </div>
                        <?php
                    }
                }
            }
            ?>

        </div>

        <div class="clear"> </div>

    </div>
</div>

<div class="clear"> </div>
<!-- End-content-gallery-->
<!-- DC Pagination:C9 Start -->
<div class="wrap">
    <!-- DC Pagination:A3 Start -->
    <ul class="dc_pagination dc_paginationA dc_paginationA03">
        <li><a href="#" class="first">First</a></li>
        <li><a href="#" class="previous">Previous</a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#" class="current">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#" class="next">Next</a></li>
        <li><a href="#" class="last">Last</a></li>

    </ul>
    <!-- DC Pagination:A3 End -->

    <div class="clear"> </div>

    <!-- DC Pagination:C9 End -->

</div>
<?php include 'includes/footer.php'; ?>