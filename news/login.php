<?php 
/*error_reporting(1);*/
include 'admin/include/config.php';
include 'admin/include/dbconnect.php';
include 'admin/include/function.php';

    session_start();
    if(isset($_COOKIE['username']) && $_COOKIE['username'] != ""){
        $_SESSION['role_id'] = 1;
        $_SESSION['name'] = $_COOKIE['username'];

        $_SESSION['success'] = "Welcome Back to system ".$_COOKIE['username'];
        @header('location: admin/dashboard.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo (isset($pagename) && $pagename != "'")? $pagename : 'Admin Template'; ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo CSS_URL;?>bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo CSS_URL;?>sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="admin/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper col-md-10">


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <?php include 'admin/include/notifications.php';?>
                	<form method="post" name="login" action="login-process.php">
						<div class="form-group">
							<label>Username</label>
							<input type="email" name="username" class="form-control" id="username" required />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control" id="username" required />
						</div>
                        <div class="form-group">
                            <input type="checkbox" name="remember_me" value="1" /> Remember Me
                        </div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-primary" id="username" required />
						</div>
                	</form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'admin/include/footer.php'; ?>