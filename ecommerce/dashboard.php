<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-06 17:27:15
 * @Organization: Knockout System Pvt. Ltd.
 */
	include 'include/config.php';
	include 'include/session.php';
	include 'include/header.php';
?>
	<div id="wrapper">
	<?php include 'include/navigation.php'; ?>
		<div class="container-fluid" style="background: white;">
			<div class="row">
				<h4>Profile Page</h4>
				<hr />
			</div>
		</div>
	</div>

<?php
	include 'include/footer.php';
?>
