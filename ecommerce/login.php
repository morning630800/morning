<?php
    session_start();
    if((isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] == 1) || (isset($_SESSION['userId']) && $_SESSION['userId'] != "") ){
        $_SESSION['success'] = "Welcome to system ".$_SESSION['name'];
        header('location: dashboard.php');
        exit;
    }

    $pageName = "Login Page";
    include 'include/config.php';

?>
<?php include 'include/header.php';?>


    <div id="wrapper">


        <div id="page-wrapper" style="max-width: 70%">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                        <?php include 'include/notifications.php'; ?>
                    	<form method="post" name="login" action="controller/login-process.php">
    						<div class="form-group">
    							<label>Username</label>
    							<input type="text" name="username" class="form-control" id="username" required />
    						</div>
    						<div class="form-group">
    							<label>Password</label>
    							<input type="password" name="password" class="form-control" id="username" required />
    						</div>
    						<div class="form-group">
    							<input type="submit" name="submit" class="btn btn-primary" id="username" required />
    						</div>
                    	</form>

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
<?php include 'include/footer.php';?>