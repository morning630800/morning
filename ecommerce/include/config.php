<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-14 07:09:15
 * @Organization: Knockout System Pvt. Ltd.
 */

define("DB_HOST",'localhost');
define("DB_USER",'root');
define("DB_PASS",'');
define("DB_NAME",'ecommerce_630');


define("SITE_URL","http://kinmel.dev/");
define("ASSETS_URL",SITE_URL."assets/");
define("CSS_URL",ASSETS_URL."css/");
define("JS_URL",ASSETS_URL."js/");
define("FONT_URL",ASSETS_URL."fonts/");
define("FONTAWESOME_URL",ASSETS_URL."font-awesome/");

define('PLUGINS_URL',ASSETS_URL."plugins/");

define('SITE_TITLE','Ecommerce 630');

?>
