<?php
    session_start();
    
    if((!isset($_SESSION['user_logged_in']) && $_SESSION['user_logged_in'] != 1) || (isset($_SESSION['userId']) && $_SESSION['userId'] == "") ){
        $_SESSION['error'] = "Please login first";
        header('location: login.php');
        exit;
    }
?>