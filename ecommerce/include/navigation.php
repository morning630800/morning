<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-06 17:18:49
 * @Organization: Knockout System Pvt. Ltd.
 */
?>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="dashboard">Ecommerce 630 Admin</a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['full_name'];?><b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li>
                    <a href="../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            <li>
                <a href="javascript:-1;" data-toggle="collapse" data-target="#demo" class="" aria-expanded="true"><i class="fa fa-fw fa-image"></i> Banner Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse" aria-expanded="true">
                    <li>
                        <a href="#">Add Banner</a>
                    </li>
                    <li>
                        <a href="#">List Banner</a>
                    </li>
                </ul>
            </li>
            <?php
                if($_SESSION['role_id'] == 1){
            ?>
            <li>
                <a href="javascript:-1;" data-toggle="collapse" data-target="#category" class="" aria-expanded="true"><i class="fa fa-fw fa-list"></i> Category Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="category" class="collapse" aria-expanded="true">
                    <li >
                        <a href="add-category">Add Category </a>
                    </li>
                    <li >
                        <a href="list-category">List Category </a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            <li>
                <a href="javascript:-1;" data-toggle="collapse" data-target="#product" class="" aria-expanded="true"><i class="fa fa-fw fa-list"></i> Product Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="product" class="collapse" aria-expanded="true">
                    <li >
                        <a href="add-product">Add Product </a>
                    </li>
                    <li >
                        <a href="list-product">List Product </a>
                    </li>
                </ul>
            </li>
            <?php
                if($_SESSION['role_id'] == 1){
            ?>
            <li>
                <a href="javascript:-1;" data-toggle="collapse" data-target="#ads" class="" aria-expanded="true"><i class="fa fa-fw fa-list"></i> Ads Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="ads" class="collapse" aria-expanded="true">
                    <li >
                        <a href="add-product">Add Advertisement </a>
                    </li>
                    <li >
                        <a href="list-product">List Advertisement </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:-1;" data-toggle="collapse" data-target="#pages" class="" aria-expanded="true"><i class="fa fa-fw fa-list"></i> pages Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="pages" class="collapse" aria-expanded="true">
                    <li >
                        <a href="add-product">Add Pages </a>
                    </li>
                    <li >
                        <a href="list-product">List Pages </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="javascript:0;" data-toggle="collapse" data-target="#user" class="" aria-expanded="true"><i class="fa fa-fw fa-list"></i> User Management <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="user" class="collapse" aria-expanded="true">
                    <li >
                        <a href="add-product">Add User </a>
                    </li>
                    <li >
                        <a href="list-product">List User </a>
                    </li>
                </ul>
            </li>
            <?php } 
                if($_SESSION['role_id'] == 1 || $_SESSION['role_id'] == 4){
            ?>
                <li>
                    <a href="order-list" class="">
                        <i class="fa fa-w fa-list"></i>
                        Order List
                    </a>
                </li>
            <?php
                }
            ?>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</nav>