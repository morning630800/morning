<?php 
	include '../include/config.php'; 
 	include '../include/session.php';
 	$pageName = "Category List";
 	$styles = '<link href="'.PLUGINS_URL.'datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css" />';

    include '../include/header.php';
 	include '../include/function.php';
    include '../Model/dbConnect.php';
    include '../Model/category.php';
?>
    <div id="wrapper">
        <?php include '../include/navigation.php';?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php include '../include/notifications.php';?>
                        <h1 class="page-header">
                            List Category
                        </h1>
						<table class="table table-responsive table-bordered table-stripped" id="category-list">
							<thead>
								<th>S.N</th>
								<th>Title</th>
								<th style="width: 30%;">Summary(Trim 150chars)</th>
								<th>Status</th>
								<th>Action</th>
							</thead>
							<tbody>
								<?php
                                    $category = new Category();
                                    $all_category = $category->getAllCategory();
                                    if($all_category){
                                        $i=1;
                                        foreach($all_category as $cat_data){
                                            ?>
                                            <tr>
                                                <td><?php echo $i;?></td>
                                                <td><?php echo $cat_data['title'];?></td>
                                                <td style="width: 30%;"><?php echo substr($cat_data['summary'],0,150)."...";?></td>
                                                <td>
                                                    <?php 
                                                        $style = "color: #00FF00;";
                                                        if($cat_data['status'] == 0){
                                                            $style = "color: #FF0000;";
                                                        }
                                                    ?>
                                                    <p style="<?php echo $style;?>"><?php 
                                                        echo getStatus($cat_data['status']);
                                                    ?>
                                                    </p>
                                                </td>
                                                <td>
                                                    <?php
                                                        $url = "../view/add-category?id=".$cat_data['id']."&act=".substr(md5('edit-'.$cat_data['id']), 4,10);
                                                    ?>
                                                    <a href="<?php echo $url;?>" class="btn btn-success" style="border-radius: 50%;"><i class="fa fa-w fa-pencil"></i></a>
                                                    
                                                    <?php
                                                    $url = "../controller/category-process?id=".$cat_data['id']."&act=".substr(md5('del-'.$cat_data['id']), 4,10);
                                                    ?>
                                                    <a href="<?php echo $url;?>" class="btn btn-danger" style="border-radius: 50%;" onclick="return confirm('Are you sure you want to delete this category?');">
                                                        <i class="fa fa-w fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                            $i++;
                                        }
                                    }
                                ?>
							</tbody>
						</table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"> 
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
$scripts = "<script type='text/javascript' src='".PLUGINS_URL."datatables/jquery.dataTables.min.js'></script>";
include '../include/footer.php';?>
<script type="text/javascript">
	$('#category-list').DataTable();
</script>