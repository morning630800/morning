<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-06 17:27:15
 * @Organization: Knockout System Pvt. Ltd.
 */
	include '../include/config.php';
	include '../include/session.php';
	include '../include/header.php';
	include '../Model/dbConnect.php';
	include '../Model/category.php';
	include '../include/function.php';
	$category = new Category();

	$act = "add";
	if(isset($_GET['act']) && isset($_GET['id']) && $_GET['act']!= "" && $_GET['id'] != ""){
		$act = "edit";
		$id = $category->sanitize($_GET['id']);
		if($_GET['act'] === substr(md5('edit-'.$id),4,10)){
			
			$data = $category->getCategoryById($id);
			
			if(!$data){
				$_SESSION['error'] = "Sorry! The data you are searching does not exists or already deleted.";
				@header('location: list-category');
				exit;		
			}

		} else {
			$_SESSION['warning'] = "Invalid Action";
			@header('location: list-category');
			exit;
		}
	}

	/*debugger($data,true);*/
?>
	<div id="wrapper">
	<?php include '../include/navigation.php'; ?>
		<div class="container-fluid" style="background: white;">
			<div class="row">
				
				<div class="col-md-10">
					<h4><?php echo ucfirst($act);?> Category</h4>
					<hr />
					<form method="post" action="../controller/category-process.php" enctype="multipart/form-data">
						<div class="form-group">
							<label for="">Title <span style="color: #FF0000;">*</span> :</label>
							<input type="text" name="category_title" required class="form-control" id="category_title" value="<?php echo (isset($data['title'])) ? $data['title'] : '';?>" />
						</div>
						<div class="form-group">
							<label for="">Summary</label>
							<textarea class="form-control" name="summary" id="summary" rows="5" style="resize: vertical;"><?php echo (isset($data['summary'])) ? $data['summary'] : '';?></textarea>
						</div>
						<div class="form-group">
							<label for="">Description</label>
							<textarea class="form-control" name="description" id="description" rows="5" style="resize: vertical;"><?php echo (isset($data['description'])) ? $data['description'] : '';?></textarea>
						</div>
						<div class="form-group">
							<label for="">Status</label>
							<select name="status" id="status" required class="form-control">
								<option value="1" <?php echo (isset($data['status']) && $data['status'] == 1) ? 'selected' : '';?>>Active</option>
								<option value="0" <?php echo (isset($data['status']) && $data['status'] == 0) ? 'selected' : '';?>>Inactive</option>
							</select>
						</div>
						<div class="form-group">
							<input type="hidden" name="category_id" value="<?php echo (isset($data['id'])) ? $data['id'] : '';?>" />
							<input type="submit" class="btn btn-primary" name="submit" value="Submit">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

<?php
	$scripts = '<script type="text/javascript" src="'.PLUGINS_URL.'tinymce/tinymce.min.js"></script><script type="text/javascript">tinymce.init({ selector:"#description" });</script>';
	include '../include/footer.php';
?>
