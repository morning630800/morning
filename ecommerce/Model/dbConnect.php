<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-14 07:08:52
 * @Organization: Knockout System Pvt. Ltd.
 */

class DbConnect{
	public $conn;
	private $db;

	public function __construct(){
		$this->conn = mysqli_connect(DB_HOST,DB_USER, DB_PASS) or die('Error in database connection');
		$this->db = $this->conn->select_db(DB_NAME) or die($this->conn->error);
	}

	public function sanitize($string){
		return $this->conn->real_escape_string($string);
	}

	protected function deleteData($table, $field, $data){
		$stmt = $this->conn->prepare("DELETE FROM ".$table." WHERE ".$field." = ?");
		if(is_int($data)){
			$stmt->bind_param('i',$data);
		} else{
			$stmt->bind_param('s',$data);
		}
		if($stmt->execute()){
			return true;
		} else {
			return false;
		}
	}
}
?>
