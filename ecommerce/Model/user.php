<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-14 07:25:14
 * @Organization: Knockout System Pvt. Ltd.
 */

class User extends DbConnect{
	public $username;
	public $password;


	public function login($username, $password){
		$this->username = $username;
		$this->password = sha1($password);

		$user = $this->getUserByUsername($this->username);
		if($user){

			if($user['password'] == $this->password){
				return $user;
			} else {
				return 2;
			}
		} else {
			return false;
		}

	}

	public function getUserByUsername($username, $isDie = false){
		$stmt = $this->conn->prepare("SELECT * FROM users WHERE username = ? AND status = 1");
		$stmt->bind_param('s',$username);
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows >= 1){
			$data =  $result->fetch_assoc();
			/*debugger($data, true);*/
			return $data;
		} else {
			return false;
		}
 	}
}
?>
