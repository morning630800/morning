<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-06 17:36:04
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
include '../include/config.php';
include '../include/function.php';
include '../Model/dbConnect.php';
include '../Model/user.php';

$user = new User();

if(isset($_POST['username']) && isset($_POST['password'])){	
	$user_form = $_POST['username'];
	$pass_form = $_POST['password'];

	/*Call to member function of User class*/
	$exist = $user->login($user_form,$pass_form);
	if($exist){
		if($exist == 2){
			$_SESSION['error'] = "Password does not match";
			@header('location: ../login.php');
			exit;	
		}

		$_SESSION['userId'] = $exist['id'];
		$_SESSION['full_name'] = $exist['full_name'];
		$_SESSION['username'] = $exist['username'];
		$_SESSION['role_id'] = $exist['role_id'];
		$_SESSION['email'] = $exist['email'];
		
		$_SESSION['user_logged_in'] = true;

		$_SESSION['success'] = "Welcome to Ecommerce 630, Backend!";
		@header("location: ../view/dashboard.php");
		exit;

	} else {
		$_SESSION['error'] = "User does not exist";
		@header('location: ../login.php');
		exit;
	}

} else {
	$_SESSION['error'] = "Invalid Access";
	header("location: ../login.php");
	exit;
}



//index.php
//Welcome admin! (when success);
//failed login: login.php
//specific message
?>
