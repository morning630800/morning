<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-06-16 07:01:24
 * @Organization: Knockout System Pvt. Ltd.
 */
include '../include/session.php';
include '../include/config.php';
include '../include/function.php';
include '../Model/dbConnect.php';
include '../Model/category.php';

$category = new Category();

if(isset($_POST['submit']) && $_POST['submit']!=""){
	/*debugger($_POST,true);*/
	$data = array();
	$data['title'] = $category->sanitize($_POST['category_title']);
	$data['summary'] =  $category->sanitize($_POST['summary']);
	$data['description'] = htmlentities($category->sanitize($_POST['description'])); //<p>& &lt;p&gt;&amp;
	$data['status'] = $category->sanitize($_POST['status']);
	$data['added_by'] = $_SESSION['userId'];
	
	$data['category_id'] = $category->sanitize($_POST['category_id']);

	if($data['category_id'] == ""){
		$act = "add";
		$last_id = $category->addCategory($data);	// This is for add category
	} else {
		$act = "edit";
		$last_id = $category->updateCategory($data);	//This is for edit category
	}
	if($last_id){
		$_SESSION['success'] = "Category ".$act."ed Successfully";
	} else {
		$_SESSION['error'] = "Sorry! There was problem while ".$act."ing category.";
	}
	@header('location: ../view/list-category.php');
	exit;
} else if(isset($_GET['act']) && isset($_GET['id'])){
	$id = $category->sanitize($_GET['id']);
	if($_GET['act'] === substr(md5('del-'.$id),4,10)){
		$data = $category->getCategoryById($id);

		if($data){
			$del = $category->deleteCategory($id);
			if($del){
				$_SESSION['success'] = "Category deleted successfully";
				@header('location: ../view/list-category.php');
				exit;	
			}else {
				$_SESSION['error'] = "Sorry! There was problem while deleting data";
				@header('location: ../view/list-category.php');
				exit;	
			}
		} else {
			$_SESSION['warning'] = "Sorry! The id you entered does not exist.";
			@header('location: ../view/list-category.php');
			exit;
		}
	} else {
		$_SESSION['warning'] = "Invalid Action";
		@header('location: ../view/list-category.php');
		exit;	
	}
	/*debugger($data,true);*/
} else {
	$_SESSION['warning'] = "Invalid Access";
	@header('location: ../view/list-category.php');
	exit;
}
?>
