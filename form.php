
<?php 
	/*
	$_GET
	$_POST
	$_REQUEST 

	//Global Variables
	$_SESSION
	$_FILES
	$_SERVER
	$_COOKIE

	*/
	if(isset($_POST['submit'])){
		/*echo $_FILES['image']['name'][0];
		echo "<br />";
		
		echo $_FILES['image']['tmp_name'][0];
		echo "<br />";

		echo $_FILES['image']['type'][0];
		echo "<br />";
		
		echo "<pre>";
		// print_r($_POST);
		print_r($_FILES);
		echo "</pre>";
		*/

		/*echo "<pre>";
		print_r($_POST);
		echo "</pre>";
		exit;*/

		$files_uploaded = array();

		$allowed_ext = array('jpg','jpeg','png','gif','doc','docx','csv','xls','xlsx','ppt','pptx','pdf');

//		exit;
		/*For multiple image Upload*/
		for($i=0; $i<count($_FILES['image']['name']); $i++){
			
			$ext = pathinfo($_FILES['image']['name'][$i],PATHINFO_EXTENSION);


			$name = "copy-".strtotime(date('ymd his')).rand(0,999999).".".$ext;

			if(in_array($ext, $allowed_ext)){
				$success = move_uploaded_file($_FILES['image']['tmp_name'][$i], "uploads/".$name);
				
				$files_uploaded[] = $name;

			}

			
		}

		echo "Files that have been uploaded: ";
		for($i=0; $i<count($files_uploaded); $i++){
			echo "<br/>";
			echo $files_uploaded[$i];
			echo "<br/>";
		}

		/*Deleting a file*/
		echo "Files that have been deleted: ";
		for($i=0; $i<count($files_uploaded); $i++){
			unlink("uploads/".$files_uploaded[$i]);
			echo "<br/>";
			echo $files_uploaded[$i];
			echo "<br/>";
		}

		/*For single Image Upload*/
		/*$ext = pathinfo($_FILES['image']['name'],PATHINFO_EXTENSION);

		$name = "file".strtotime(date('ymd his')).rand(0,999999).".".$ext;

		$success = move_uploaded_file($_FILES['image']['tmp_name'], "uploads/".$name);*/
		


		//$success = move_uploaded_file($_FILES['image']['tmp_name'], "uploads/".$_FILES['image']['name']);


		exit;		
		//header("location: form.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Form Page</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
	<script type="text/javascript" src="assets/js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<style type="text/css">
		.width-fixed {
			max-width: 200px !important;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<form action="form.php" method="post" enctype="multipart/form-data" name="" id="" class="form form-horizontal">
					<div class="form-group">
						<label for="username">UserName: </label>
						<input type="email" id="username" name="username[]" class="form-control width-fixed" placeholder="Please insert your email address as a username"  />
					</div>

					<div class="form-group">
						<label for="username">UserName: </label>
						<input type="email" id="username" name="username[]" class="form-control width-fixed" placeholder="Please insert your email address as a username" length="160" />
					</div>
					<div class="form-group">
						<label>Places You have visited:</label>
						<input type="checkbox" name="places[]" value="Kathmandu" /> Kathmandu
						<input type="checkbox" name="places[]" value="Pokhara" /> Pokhara
						<input type="checkbox" name="places[]" value="Mustang" /> Mustang
						<input type="checkbox" name="places[]" value="Mugu"> Mugu

					</div>

					<div class="form-group">
						<label>Gender</label>
						<input type="radio" name="gender" value="Male" /> Male
						<input type="radio" name="gender" value="Female" /> Female
						<input type="radio" name="gender" value="Other" /> Other
					</div>
					<div class="form-group">
						<label>Select Country: </label>
						<select name="country" id="coutry" class="form-control">
							<option value="nepal">Nepal</option>
							<option value="usa">USA</option>
							<option value="uk" selected >UK</option>
						</select>
					</div>
					<div class="form-group">
						<label>Description: </label>
						<textarea name="description" class="form-control description" rows="5" cols="30" style="resize: vertical;" ></textarea>
					</div>
					
					<div class="form-group">
						<label>File Upload: </label>
						<input type="file" name="image[]" multiple accept="*/image" />
					</div>

					<div class="form-group">
						<input type="submit" class="btn btn-default" name="submit" value="Submit" />
						<input type="hidden" name="id" value="123" />
						<!-- <input type="submit" class="btn btn-primary" />
						<input type="submit" class="btn btn-success" />
						<input type="submit" class="btn btn-warning" />
						<input type="submit" class="btn btn-info" />
						<input type="reset" class="btn btn-danger" />
						<button type="submit">Submit</button> -->
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
