<?php 
    session_start();
    if(isset($_COOKIE['username']) && $_COOKIE['username'] != ""){
        $_SESSION['role_id'] = 1;
        $_SESSION['name'] = $_COOKIE['username'];

        $_SESSION['success'] = "Welcome Back to system ".$_COOKIE['username'];
        @header('location: dashboard.php');
        exit;
    }

    include 'include/header.php';
?>

    <div id="wrapper col-md-10">


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <?php include 'include/notifications.php';?>
                	<form method="post" name="login" action="login-process.php">
						<div class="form-group">
							<label>Username</label>
							<input type="email" name="username" class="form-control" id="username" required />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" class="form-control" id="username" required />
						</div>
                        <div class="form-group">
                            <input type="checkbox" name="remember_me" value="1" /> Remember Me
                        </div>
						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-primary" id="username" required />
						</div>
                	</form>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php include 'include/footer.php'; ?>