<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-21 07:04:21
 * @Organization: Knockout System Pvt. Ltd.
 */
function debugger($array, $isDie = false){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
	if($isDie){
		exit;
	}
}

function argument_by_value($value){
	$value = $value+1;
	return $value;
}


function volume_calculate($length, $width=0, $height=0){

	if($width > 0 && $height > 0){
		$volume = $length*$width*$height;
	} else {
		$volume = $length*$length*$length;
	}

	return $volume;
}


function current_time(){
	return date('Y-M-d H:i:s A');
}

function previous_date($days){
	$xyz = strtotime(date('Y-m-d H:i:s A')." -".$days." days");

	$date = date('Y-m-dS z H:i:s A',$xyz);
	return $date;
}

function sanitizeString($string){
	global $conn;
	return mysqli_real_escape_string($conn,$string);
}

function getUserByUsername($username){
	global $conn;
	$sql = "SELECT * FROM users WHERE username = '".$username."'";
	$query = mysqli_query($conn,$sql);

	if(mysqli_num_rows($query) > 0){
		$data = mysqli_fetch_assoc($query);
		return $data;
	} else {
		return false;
	}
}

function userLoginUpdateById($userId){
	global $conn;
	$sql = "UPDATE users SET last_login = now(), login_ip = '".$_SERVER['REMOTE_ADDR']."'";
	$query = mysqli_query($conn,$sql);
	if($query){
		return true;
	} else {
		return false;
	}
}

function addUser($data, $isDie=false){
	global $conn;
	$sql = "INSERT INTO users SET full_name = '".$data['full_name']."', username = '".$data['username']."', password = '".$data['password']."', status = ".$data['status'].", role_id = ".$data['role_id'].", added_date = now() ";
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn,$sql);
	if($query){
		return mysqli_insert_id($conn);
	} else {
		return false;
	}
}

function getAllUser($isDie=false){
	global $conn;
	$sql = "SELECT * FROM users ORDER BY id DESC";
	if($isDie){
		echo $sql;
		exit;
	}
	$query = mysqli_query($conn,$sql);
	if(mysqli_num_rows($query) <= 0){
		return false;
	} else {
		$data = array();
		while($row = mysqli_fetch_assoc($query)){
			$data[] = $row;
		}
		return $data;
	}

}

function getUserType($role_id){
	if($role_id == 1){
		return "Admin";
	} else if($role_id == 2){
		return "User";
	} else {
		return "User type not defined";
	}
}

function getStatus($status){
	if($status == 1){
		return "Active";
	} else if($status == 0){
		return "Inactive";
	} else {
		return "Suspended";
	}
}
?>
