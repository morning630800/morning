<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-28 07:23:02
 * @Organization: Knockout System Pvt. Ltd.
 */
$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD) OR die("Error in database Connection");
$db = mysqli_select_db($conn, DB_NAME) OR die(mysqli_error($conn));
?>
