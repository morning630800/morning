<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-28 07:15:53
 * @Organization: Knockout System Pvt. Ltd.
 */
//127.0.0.1 ::1

if($_SERVER['SERVER_ADDR'] == "127.0.0.1" || $_SERVER['SERVER_ADDR'] == "::1" ){
	define('ENVIRONMENT','DEVELOPMENT');
} else {
	define('ENVIRONMENT','PRODUCTION');
}

if(ENVIRONMENT == "DEVELOPMENT"){
	define('DB_HOST','localhost');
	define('DB_USER','root');
	define('DB_PASSWORD','');
	define('DB_NAME','custom_cms');


	define('SITE_URL','http://localhost/custom_cms/');
	define('ASSETS_URL',SITE_URL."assets/");
	define('CSS_URL',ASSETS_URL."css/");
	define('JS_URL',ASSETS_URL."js/");
	define('FONTS_URL',ASSETS_URL."fonts/");
} else {
	define('DB_HOST','localhost');
	define('DB_USER','asdfasdf');
	define('DB_PASSWORD','asdfas');
	define('DB_NAME','custom_cms');


	define('SITE_URL','http://test.com/');
	define('ASSETS_URL',SITE_URL."assets/");
	define('CSS_URL',ASSETS_URL."css/");
	define('JS_URL',ASSETS_URL."js/");
	define('FONTS_URL',ASSETS_URL."fonts/");
}
?>
