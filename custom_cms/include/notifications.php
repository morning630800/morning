<?php
    if(isset($_SESSION['error']) && $_SESSION['error'] != ""){
        ?>
        <p class="alert alert-danger">
          <?php echo $_SESSION['error']; ?>  
        </p>
        <?php 
    }
    $_SESSION['error'] = "";

	if(isset($_SESSION['success']) && $_SESSION['success'] != ""){
        ?>
        <p class="alert alert-success">
          <?php echo $_SESSION['success']; ?>  
        </p>
        <?php 
    }
    $_SESSION['success'] = "";

	if(isset($_SESSION['warning']) && $_SESSION['warning'] != ""){
        ?>
        <p class="alert alert-warning">
          <?php echo $_SESSION['warning']; ?>  
        </p>
        <?php 
    }
    $_SESSION['warning'] = "";

	if(isset($_SESSION['info']) && $_SESSION['info'] != ""){
        ?>
        <p class="alert alert-info">
          <?php echo $_SESSION['info']; ?>  
        </p>
        <?php 
    }
    $_SESSION['info'] = "";

?>