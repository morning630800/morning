<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-21 07:18:46
 * @Organization: Knockout System Pvt. Ltd.
 */

include 'include/function.php';

/*function argument_by_value($value){
	$value = $value+1;
	return $value;
}

function argument_by_reference(&$x){
	$x = $x+1;
}
*/

$value = 123;
/*$return_data = argument_by_value($value);
echo $return_data;
echo "<br/>";
echo $value;*/

/*echo "<br/>";
echo $value;
argument_by_reference($value);
echo "<br/>";
echo $value;*/

//echo current_time();
echo previous_date(23);
echo "<br/>";


$string = "lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed Lorem accusamus, eveniet asperiores cupiditate non nemo ut perferendis velit Lorem voluptatem veniam temporibus sequi Lorem blanditiis iste fugit quae nulla numquam quis veritatis.";

$replace = str_replace("Lorem", "PHP", $string);
echo $replace;
echo "<br/>";
$check = strpos($string, " ");
echo (int)$check;
echo "<br/>";
$substring = substr($string, -10,10);
echo $substring;
$length = strlen($string);
$sub_string = substr($string, ceil($length/2));
echo "<br/>";
echo $sub_string;

echo "<br/>";
echo ucfirst($string);
echo "<br/>";

echo ucwords($string);

echo "<br/>";

echo strtoupper($string);
echo "<br/>";

echo str_replace("Lorem", "TEST", (ucwords(strtolower($string))));


$var1 = 12;
$var2 = "is";
$var3 = "data";
$var = "pirntf";
printf("%d %s %s",$var1,$var2,$var3);
echo "<br/>";

//%s = string
//%d = decimal
//%i = unsigned


$text = sprintf("This is %s data",$var);
echo $text;

/*ini_set("SMTP",'smtp.wlink.com.np');
ini_set("smtp_port",25);

php mailer
swiftmailer

*/

ini_set('display_errors',false);

$header = "";
$header .= "MIME-Version: 1.0 \r\n";
$header .= "Content-type: text/html; charset: iso-8859-1 \r\n";
$header .= "X-Priority: 1 \r\n";
$header .= "From: admin@demo.bhattaraisandesh.com.np";


$to = "sandesh.bhattarai79@gmail.com";
$subject = "This is test message";
$Message = "This is a test message sent from localhost. ";
$Message .= "Click <a href='http://localhost/custom_cms/index.php'>Here</a> <img src='http://localhost/custom_cms/assets/images/test.png' />";


$var = mail($to, $subject, $Message, $header);
if($var){
	//
} else {
	//
}
?>
