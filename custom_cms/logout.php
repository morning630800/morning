<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-18 07:42:33
 * @Organization: Knockout System Pvt. Ltd.
 */
session_start();
session_destroy();

setcookie('username','',time()-60);

@header('location: login.php');
exit;
?>
