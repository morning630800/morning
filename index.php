<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-09 07:00:54
 * @Organization: Knockout System Pvt. Ltd.
 */
	 	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>HTML and PHP within each other's Code</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<script type="text/javascript" src="assets/js/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

</head>
<body>
	<!-- Body of html -->
	<div class="container" id="container1">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
				<table class="table table-bordered table-striped">
					<thead>
						<th>S.N</th>
						<th>Student Name</th>
						<th>Class</th>
						<th>Roll No.</th>
						<th>Address</th>
						<th>Email Address</th>
						<th>Phone Number</th>
					</thead>
					<tbody>
						<?php
							if(isset($student)){
								$i =1;
								foreach($student as $key => $value){
									/*echo '<tr>';
									echo '</tr>';*/
								?>
									<tr>
										<td><?php echo $i;?></td>
										<td><?php echo $value['name'];?></td>
										
										<td <?php echo($i==4) ?' colspan="2"':"" ;?> ><?php echo $value['class'];?></td>
										
										<?php 
										if($i!=4){
										?>
										<td><?php echo $value['roll_no'];?></td>
										<?php }
										?> 	
										<td><?php echo $value['address'];?></td>
										<td><?php echo $value['email_address'];?></td>
										<td><?php echo $value['phone_number'];?></td>
									</tr>
								<?php 
								$i++;
								}
							} else {
								?>
								<tr>
									<!-- <td></td>
									<td></td>
									<td></td>
									<td colspan="2"></td> -->
									<td colspan="7">Sorry! There was no any data.</td>
								</tr>
								<?php
							}
						?>
					</tbody>
					<tfoot></tfoot>
				</table>
			</div>
		</div>
	</div>
</body>
</html>
