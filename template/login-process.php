<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-17 07:25:45
 * @Organization: Knockout System Pvt. Ltd.
 */
ob_start();
session_start();

$user = "admin@test.com";
$password = "admin";

if(isset($_POST['submit']) && $_POST['submit']!=""){
	/*Login validation codes*/
	$post_username = $_POST['username'];
	$post_password = $_POST['password'];

	if($user == $post_username){
		if($password == $post_password){
			echo "User credential Matched";
		} else {
			echo "Password does not match";
		}
	} else {
		echo "Username does not matched";
	}
} else {
	header('Location: login.php');
	exit;
}
ob_flush();
?>
