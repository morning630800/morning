<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-07 07:04:50
 * @Organization: Knockout System Pvt. Ltd.
 */

/*Loop*/
/*While Loop*/
echo "While Loop <hr>";
$x = 11; 
while($x <= 10){
	echo $x;
	echo "<br />";
	// $x = $x+1;
	// $x += 1;
	$x++;
}

echo "<hr />Do while Loop <hr />";
$x = 11;
do{
	echo $x;
	echo "<br/>";
	$x++;
} while($x<=10);

echo "<hr />For Loop<hr />";
for($i=1; $i<=10; $i++){
	/*echo $i;
	echo "<br/>";*/
}

$multi_dimension = array(
					"car" =>	array(	"company" => "honda",
								"color" => "black",
								"engine" => array("5L","1.2L","3L"),
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "honda",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "honda",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "honda",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "honda",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),

						);
/*Foreach*/
echo "<hr>Foreach<hr>";
$i=1;
$obje = $multi_dimension[10];

foreach($multi_dimension as $key => $value){
	if($key == 0){
		echo $value['company']." (Engine Capacity): ".$value['engine'][0];
		echo "<br/>";
	}
	/*
		$.each(data,function(index,value){
		//Javascript syntax of foreach Loop
		});
	*/

	/*foreach($value as $key1 => $info){
		echo $key1." => ".$info;
		echo "<br/>";
	}
		echo "<br/>";*/
	
}
/*echo "<pre>";
print_r($multi_dimension);
echo "</pre>";*/
?>
