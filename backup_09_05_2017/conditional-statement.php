<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-04 07:01:26
 * @Organization: Knockout System Pvt. Ltd.
 */

/*If condition*/
$x = 4;
$y = 5;
echo "If Condition";
echo "<hr />";
echo "<br/>";

if($x == 4):
	echo $x;
endif;
echo "<br/>";

if($x==4){
	echo $x;
}
echo "<hr />";
echo "If else Condition";
echo "<hr />";
echo "<br/>";
if($x<0){
	echo '$x is less then 0. Value: '.$x;
} else {
	echo '$x is greater then 0. Value: '.$x;
}

echo "<br />";

if($x<0):
		echo '$x is less then 0. Value: '.$x;
	else:
		echo '$x is greater then 0. Value: '.$x;
endif;

echo "<hr />";
echo "else if Condition";
echo "<hr />";
echo "<br/>";

if($x==0){
	
	echo '$x is equal to 0';
} else if($x >0){
	echo '$x is greater than 0';
} else {
	echo '$x is less than to 0';
}


echo "<br />";
if($x==0):
	echo '$x is equal to 0';
elseif($x >0):
	echo '$x is greater than 0';
else:
	echo '$x is less than to 0';
endif;


/*Switch Cases*/

echo "<hr />";
echo "Switch Statements";
echo "<hr />";
switch ($x) {
	case 0:
		echo '$x is equal to 0';
		break;
	case 1:
		echo '$x is equal to 1';
		break;
	case 2:
		echo '$x is equal to 2';
		break;
	case 3:
		echo '$x is equal to 3';
		break;
	case 4:
		echo '$x is equal to 4';
		break;
	case 5:
		echo '$x is equal to 5';
		break;
	default:
		echo '$x is greater than 5 or less than 0';
		break;
}

?>
