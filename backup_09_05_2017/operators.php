<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-08 07:26:12
 * @Organization: Knockout System Pvt. Ltd.
 */
/*Assignment Operators*/
$string = "String value";

/*Arithmetic Operators*/
/* + - * / % ++ -- */
$x = 1;
$x = $x+1;
$x += 1;
$x++;


/*Comparison Operator*/
/* ==, ===, >, <, >=, <=, !=, !==*/

$string = '123';
$number = 123;

if($string == $number){
	echo "both are equal. <br/>";
}
//both are equal


if($string === $number){
	echo "both are again equal. <br/>";
} else {
	echo "Both are not equal.";
}

/*Both are not equal*/

/*Increment/ Decrement Operator*/
/*++, --*/
$a= 10;
$a++;
//11
$a--;
//10

/*String Operators*/
/* . */
$string = "Hello"." "."world".$x." ".$a;


/*Logical Operatos*/
/* &&, ||, AND, OR , ! */
/*if(!empty() && !isset()){

}*/

/*Conditional Operator (or ternary )*/
/* (condition) ? true : false; */

echo ($x>10) ? "X is greater than 10" : "X is less than 10";

$str = ($x>10) ? "X is greater than 10" : "X is less than 10";
echo $str;

/*Error Supressor*/
/*@header();
$mail_sent = @mail();
*/
?>
