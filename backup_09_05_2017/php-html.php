<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-02 07:02:49
 * @Organization: Knockout System Pvt. Ltd.
 */
$text = "This is paragraph";

$html_tags = "<!DOCTYPE html>";
$html_tags .= "<html><head><title>Php within Html</title></head><body><p>";
$html_tags .= $text;
$html_tags .= "</p></body></html>"; 

//echo $html_tags;
?>



<!DOCTYPE html>
<html>
<head>
	<title>PHP within HTML</title>
</head>
<body>
	<p>
		<?php 
			echo $text; 
		?>
	</p>

	<div>
		this is div tag
		<?php 
			echo "this is another div tag"; 
		?>
		<pre>
		</pre>
	</div>
</body>
</html>
