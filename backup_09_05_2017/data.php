<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-01 06:58:23
 * @Organization: Knockout System Pvt. Ltd.
 */

//Data types
/*1. String Data type*/
$string = "Hello ";	//String data type
$string_1 = ' Class.';	//String data type

echo $string;
echo $string_1;

//Out put
//Hello Class.
echo "<br />";

echo ' $string_1 ';	//$string_1
echo " $string_1 ";	//Class.

echo "<br />";

/*Concating Strings*/
echo $string.$string_1; //String Concatination by dot(.) operator


/*Number Types*/
$num  = 9802111635;	//integer -2,147,483,648 - 2,147,483,647
$num_1 = 12.3;	//Float
$num_2 = 22/7;	//flaot
echo "<br/>";
echo "<br/>";
echo "<br/>";
echo "Number type";
echo "<br/>";
echo $num;
echo "<br/>";
echo $num_1;
echo "<br/>";
echo $num_2;
echo "<br/>";


/*Boolean Data type*/
$bool = true;	//integer conversion 1 or true
$bool_1 = false; 	//blank or 0 or false

echo "<br/>";
echo "Boolean Data Type";
echo "<br/>";
echo $bool;
echo "<br/>";
echo (int)$bool_1;
echo "<br/>";

/*Array Data type*/
echo "<br/>";
echo "Array Data Types";
echo "<br/>";
$array = array(1,2,3,4,5,6); //array always stored in key=>value pair

echo $array[0];	
echo "<br/>";
echo $array[4];


/*Object*/
class Student{
	public $student_name;
	public $student_email;
	public function __construct(){
		return $this;
	}

}
echo "<br/>";

$obj = new Student();
print_r($obj);

var_dump($obj);

/*Null data type*/
$null_data_type = null;


?>