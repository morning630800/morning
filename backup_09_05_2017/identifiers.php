<?php /**
 * @Author: Sandesh Bhattarai
 * @Date:   2017-05-02 07:35:33
 * @Organization: Knockout System Pvt. Ltd.
 */

/*Variables*/
/*$num =10;
echo $num;
echo "<br />";

$num = 20;
echo $num;
echo "<br />";*/

/*Constant*/
/*const NAME = "test";	//Class constant
echo NAME;
echo "<br />";

//NAME = "TEST 1"; 	//Error

define("NAME_1","Test one");	//Outside class
echo NAME_1;
echo "<br/>";
echo "<br/>";
echo "<br/>";
echo "<br/>";
echo "<br/>";*/



/*Array*/
/*Indexive Array*/
/*$arr = array(1,2,3);
echo $arr[2];	//3
echo "<br />";

$arr = array("a",2,3);
echo $arr[0]; //a
echo "<br />";*/

/*Associative Type of array*/
/*$arr = array(
			"first"	=>	"a",
			"second"=>	2,
			"third"	=>	3);
echo $arr['second']; //2
echo "<br />";


$arr = array(
			array(1,2,3), "Lamborgini","Honda",10000);
echo $arr[0][1]; //2
echo "<br />";

$arr = array(
			array(
				array("a","b","c"),
				"car_1"=>"Lamborgini",
				"car_2" => "Honda",
				10000
				)
			);
echo $arr[0][0][0]; //a
echo "<br/>";
echo $arr[0]['car_2'];	//Honda


*/


$single_dimension = array(7,4,12,14,3,1,6,'a','z','b');

$multi_dimension = array(
						array(	"company" => "honda",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),
						array(	"company" => "Lamborgini",
								"color" => "black",
								"engine" => "6L",
								"capacity" => 6000,
								"seat" => 4),
						array(	"company" => "BMW",
								"color" => "black",
								"engine" => "5L",
								"capacity" => 5000,
								"seat" => 2),

						);
$multi_dimension_1 = array(	"company" => "Hyundai",
								"color" => "black",
								"engine" => "1.2L",
								"capacity" => 1200,
								"seat" => 4
							);
/*echo "<pre>";
echo "<p>Before Sorting</p>";
print_r($single_dimension);
echo "</pre>";*/

sort($single_dimension);	//Array Sort in ASC order
/*
echo "<pre>";
print_r($single_dimension);
echo "</pre>";*/


$count = count($single_dimension); //Get the length of array
/*echo $count;
echo "<br/>";
echo count($multi_dimension);

echo "<br/> ";

echo "Array Shift";
echo "<br/> ";
*/$first_element = array_shift($multi_dimension);

/*echo "<pre>";
echo "First Element";
print_r($first_element);
echo "New multi_dimension array";
print_r($multi_dimension);
echo "</pre>";
echo "Array Pop";
echo "<br/> ";*/
$last_element = array_pop($multi_dimension);
/*echo "<pre>";
print_r($last_element);
echo "New multi_dimension array";
print_r($multi_dimension);
echo "</pre>";*/


echo "Inserting into array";
$array = array();
$array[] =1; //0
$array[] =5; //1
$array[] =14; //2

$array['name'] = "Sandesh";
$array[] = "Bhattarai";

/*echo "<pre>";
print_r($array);
echo "</pre>";
*/$arr = array(1,2,3,4,5);
array_push($array, 1);

/*echo "<pre>";
print_r($array);
echo "</pre>";
*/
$string = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis minus sequi laboriosam consequuntur reiciendis, esse, eius eveniet porro deserunt iste optio, rem facilis. Sunt sit nobis odit quos cum voluptatum! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Obcaecati, numquam laborum. Voluptatem nihil cupiditate ipsum quae ut iusto nam minus ipsa, vitae. Deleniti deserunt aut iusto dolorem, labore dolorum porro!";

$list = explode(" ",$string); //String to array
// echo "<pre>";
// print_r($list);
// echo "</pre>";

$string_converted = implode("",$list);
echo "<pre>";
print_r($string_converted);
echo "</pre>";

$serialized_data = serialize($list);
$unserialized_data = unserialize($serialized_data);
echo "<pre>";
print_r($serialized_data);
print_r($unserialized_data);
echo "</pre>";

$json_data = json_encode($list);
$json_decoded_data = json_decode($json_data);
echo "<pre>";
print_r($json_data);
print_r($json_decoded_data);
echo "</pre>";


/*Array Merge*/
$num1 = 12.3456790;
echo (int)$num1;

$multi_dimension_1 = array(array($multi_dimension_1));
$new_array = array_merge($multi_dimension,$multi_dimension_1);
echo "<pre>";
print_r($new_array);
echo "</pre>";
?>
